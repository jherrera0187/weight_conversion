﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Weight_Conversion
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            double Pound = (double)numericUpDown1.Value;
            double Answer = Pound * 0.453592;
            Answer = Math.Round(Answer, 3, MidpointRounding.AwayFromZero);
            label3.Text = Answer.ToString() + "KGs";
        }

        private void button2_Click(object sender, EventArgs e)
        {
            double KG = (double)numericUpDown2.Value;
            double Answer2 = KG * 2.20462;
            Answer2 = Math.Round(Answer2, 3, MidpointRounding.AwayFromZero);
            label3.Text = Answer2.ToString() + "Pounds";
        }

        private void button3_Click(object sender, EventArgs e)
        {
            numericUpDown1.Value = 0;
            numericUpDown2.Value = 0;
            label3.Text = "Answer";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
